//Copyright C.2020 Jorge Meneu Moreno, Inc bitbucket.org/Jorge_Meneu_Moreno

#define N 50

#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char* argv[]) {
    mkfifo("/tmp/logger_fifo",0777);
    int fd=open("/tmp/logger_fifo", O_RDONLY);
    int loop=0;
    int nbytes;
    while(loop==0)
    {
        char buffer[N];
        nbytes=read(fd,buffer,N);
        if(nbytes<0){
         perror("ERROR");
         return 1;
        }
        printf("%s\n", buffer);
        if((buffer[0]=='-'))         {
            loop=1; 
        }
        if(nbytes==0)
        {
        	loop = 1;
        }

    }
    close(fd);
    unlink("/tmp/logger_fifo");
    return 0;
}


