//Copyright C.2020 Jorge Meneu Moreno, Inc bitbucket.org/Jorge_Meneu_Moreno

// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{       
        radio=0.1f;
        velocidad.x=3;
        velocidad.y=3;
        radio_min=0.1f;
        radio_max=0.5f;
        s_pulso = 0.03f;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
        glColor3ub(255,255,0);
        glEnable(GL_LIGHTING);
        glPushMatrix();
        glTranslatef(centro.x,centro.y,0);
        glutSolidSphere(radio,15,5);
        glPopMatrix();
}

void Esfera::Mueve(float t)
{
centro = centro + velocidad*t;

}
