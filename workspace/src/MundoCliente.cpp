//Copyright C.2020 Jorge Meneu Moreno, Inc bitbucket.org/Jorge_Meneu_Moreno

//MundoCliente.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#define N 75
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <pthread.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
char* projection;

CMundo::CMundo()
{
    Init();
}

CMundo::~CMundo()
{
    char endgame[N];
    sprintf(endgame,"----GAME OVER...---");
    write(pipe,endgame,strlen(endgame)+1);
    close(pipe);
    munmap(projection,sizeof(MComp));
}

void CMundo::InitGL()
{
    //Habilitamos las luces, la renderizacion y el color de los materiales
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    
    glMatrixMode(GL_PROJECTION);
    gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
    glDisable (GL_LIGHTING);

    glMatrixMode(GL_TEXTURE);
    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
    glColor3f(r,g,b);
    glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
    int len = strlen (mensaje );
    for (int i = 0; i < len; i++) 
        glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
        
    glMatrixMode(GL_TEXTURE);
    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
    //Borrado de la pantalla    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Para definir el punto de vista
    glMatrixMode(GL_MODELVIEW); 
    glLoadIdentity();
    
    gluLookAt(0.0, 0, 17,  // posicion del ojo
        0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
        0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

    /////////////////
    ///////////
    //      AQUI EMPIEZA MI DIBUJO
    char cad[N];
    sprintf(cad,"J1: %d",puntos1);
    print(cad,10,0,1,1,1);
    sprintf(cad,"J2: %d",puntos2);
    print(cad,650,0,1,1,1);
    int i;
    for(i=0;i<paredes.size();i++)
        paredes[i].Dibuja();

    fondo_izq.Dibuja();
    fondo_dcho.Dibuja();
    jugador1.Dibuja();
    jugador2.Dibuja();
    esfera.Dibuja();

    /////////////////
    ///////////
    //      AQUI TERMINA MI DIBUJO
    ////////////////////////////

    //Al final, cambiar el buffer
    glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{   
    jugador1.Mueve(0.025f);
    jugador2.Mueve(0.025f);
    esfera.Mueve(0.025f);

    int i;
    for(i=0;i<paredes.size();i++)
    {
        paredes[i].Rebota(esfera);
        paredes[i].Rebota(jugador1);
        paredes[i].Rebota(jugador2);
    }

    jugador1.Rebota(esfera);
    jugador2.Rebota(esfera);
    if(fondo_izq.Rebota(esfera))
    {
        esfera.centro.x=0;
        esfera.centro.y=rand()/(float)RAND_MAX;
        esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
        esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
        puntos2++;

        char str[N];//pipe point for J2
        sprintf(str,"J2 SCORES 1 POINT!\nJ1 | J2\n %d | %d\n", puntos1, puntos2);
        write(pipe,str,strlen(str)+1);
    }

    if(fondo_dcho.Rebota(esfera))
    {
        esfera.centro.x=0;
        esfera.centro.y=rand()/(float)RAND_MAX;
        esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
        esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
        puntos1++;

        char str[N];//pipe point for J1
        sprintf(str,"J1 SCORES 1 POINT!\nJ1 | J2\n %d | %d\n", puntos1, puntos2);
        write(pipe,str,strlen(str)+1);
    }
    pMComp->esfera=esfera;//update projection data
    pMComp->raqueta1=jugador1;//update projection data
    

    switch(pMComp->accion)
    {
        case 1: jugador1.velocidad.y=4;             
            break;
        case 0: break;
        case -1: jugador1.velocidad.y=-4;
            break;
    }


    int EstadoActual=0;
    int EstadoAnterior=0;
    char str[N];
    if(puntos1==3)
    {
        EstadoActual=1;
        if(EstadoActual!=EstadoAnterior)
        {
            sprintf(str,"J1 IS THE WINNER!\n");
            write(pipe,str,strlen(str)+1);
        }
        EstadoAnterior=EstadoActual;
        exit(0);
    }
    if(puntos2==3)
    {
        EstadoActual=1;
        if(EstadoActual!=EstadoAnterior)
        {
            sprintf(str,"J2 IS THE WINNER!\n");
            write(pipe,str,strlen(str)+1);
        }
        EstadoAnterior=EstadoActual;
        exit(0);
    }
    
    char coordinates[N];
    s_communication.Receive(coordinates,sizeof(coordinates));
    sscanf(coordinates, "%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x, &esfera.centro.y, &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2,&jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2);

    
        
}
void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{

    char tecla[]="0";
    switch(key)
    {
    case 's':sprintf(tecla, "s");break;
    case 'w':sprintf(tecla, "w");break;
    case 'l':sprintf(tecla, "l");
  
        break;
    case 'o':sprintf(tecla,"o");
  
        break;
    }
    s_communication.Send(tecla, sizeof(tecla));
}


void CMundo::Init()
{

char IP[]="127.0.0.1";
char name[N];
printf("NAME: ");
scanf("%s", name);
s_communication.Send(name, sizeof(name));


    Plano p;
//pared inferior
    p.x1=-7;p.y1=-5;
    p.x2=7;p.y2=-5;
    paredes.push_back(p);

//superior
    p.x1=-7;p.y1=5;
    p.x2=7;p.y2=5;
    paredes.push_back(p);

    fondo_izq.r=0;
    fondo_izq.x1=-7;fondo_izq.y1=-5;
    fondo_izq.x2=-7;fondo_izq.y2=5;

    fondo_dcho.r=0;
    fondo_dcho.x1=7;fondo_dcho.y1=-5;
    fondo_dcho.x2=7;fondo_dcho.y2=5;

    //a la izq
    jugador1.g=0;
    jugador1.x1=-6;jugador1.y1=-1;
    jugador1.x2=-6;jugador1.y2=1;

    //a la dcha
    jugador2.g=0;
    jugador2.x1=6;jugador2.y1=-1;
    jugador2.x2=6;jugador2.y2=1;

    //logger implementation
    pipe=open("/tmp/logger_fifo",O_WRONLY);//pipe creation, mode: WRONLY

    //bot implementation
    int fd=open("/tmp/bot_data.txt",O_RDWR|O_CREAT|O_TRUNC, 0777); //file creation
    if(fd<0)
    {
    	perror("ERROR. CORRUPT FD!");
    	exit(1);
    }
    write(fd,&MComp,sizeof(MComp));//file writing
    projection=(char*)mmap(NULL,sizeof(MComp),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0); //file memory mapping
    if(projection==MAP_FAILED){
    
    perror("ERROR. FAULTY MEMORY MAPPING!");
    exit(1);
    
    }
    close(fd); //file closing after succesfull memory mapping
    pMComp=(DatosMemCompartida*)projection;
    pMComp->accion=0; //actions per racket
    


}
