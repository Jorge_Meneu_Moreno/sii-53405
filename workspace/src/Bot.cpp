//Copyright C.2020 Jorge Meneu Moreno, Inc bitbucket.org/Jorge_Meneu_Moreno

#define sleep_time 25000

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()

{
    int fd;
    DatosMemCompartida* pMComp;
    char* projection;

    fd=open("/tmp/bot_data.txt",O_RDWR);
    if(fd<0){
    perror("ERROR. CORRUPT FD!");
    return 1;
    
    }

    projection=(char*)mmap(NULL,sizeof(*(pMComp)),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0);

    close(fd);

    pMComp=(DatosMemCompartida*)projection;


    while(1)
    {
        usleep(sleep_time);  
        float posRaqueta1;
        posRaqueta1=((pMComp->raqueta1.y2+pMComp->raqueta1.y1)/2);
        if(posRaqueta1<pMComp->esfera.centro.y)
            pMComp->accion=1;
        else if(posRaqueta1>pMComp->esfera.centro.y)
            pMComp->accion=-1;
        else
            pMComp->accion=0;  
    }

    munmap(projection,sizeof(*(pMComp)));

}
