//Copyright C.2020 Jorge Meneu Moreno, Inc bitbucket.org/Jorge_Meneu_Moreno

// MundoServidor.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_


#if _MSC_VER > 1000
#pragma once
#endif 

#include <vector>
#include "Plano.h"
#include "Esfera.h"
#include "Raqueta.h"

#include <stdio.h>//header files for pipe creation and handling
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "DatosMemCompartida.h"//header files for sharing memory
#include <sys/mman.h>
#include<pthread.h>

#include "Socket.h"

class CMundo  

{
public:
    void Init();
    CMundo();
    virtual ~CMundo();  
    
    void InitGL();  
    void OnKeyboardDown(unsigned char key, int x, int y);
    void OnTimer(int value);
    void OnDraw();
    void RecibeComandosJugador();  

    Esfera esfera;
    std::vector<Plano> paredes;
    Plano fondo_izq;
    Plano fondo_dcho;
    Raqueta jugador1;
    Raqueta jugador2;

    int puntos1;
    int puntos2;

    int pipe;//pipe creation
    
    DatosMemCompartida MComp;
    DatosMemCompartida* pMComp;//pointer for shared memory
    
    
    pthread_t thid1;
    
    Socket s_connection;
    Socket s_communication;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)



