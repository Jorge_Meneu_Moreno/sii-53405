######	//Copyright C.2020 Jorge Meneu Moreno, Inc bitbucket.org/Jorge_Meneu_Moreno

##	CHANGELOG
***


>VERSIONS

*	V5.X-- 22/12/2020.

	+	Improved Server & Client functionalities, adding the ability to play an online match in real time.

	+	Implemented Sockets to enable online interaction. 
	+	Modified CMakelists.txt to enable Socket execution.
	+	Updated controlls between client and server and keyboard input.


*	V4.X-- 08/12/2020.

	+	Implemented Server & Client functionalities, adding the ability to have separate screens updated in real time.

	+	Erased Mundo.cpp & mundo.h, substituted with MundoServidor.cpp/MundoServidor.h & MundoCliente.pp/MundoCliente.h. 
	+	Modified CMakelists.txt to enable Server and Client execution.
	+	New changes after updated, merged to master branch.


*	V3.X-- 26/11/2020.

	+	Added Scoreboard functionality. Logger.cpp was made using a pipe, to exchange and print information related to scores during gameplay.

	+	Added CPU game mode. Bot.cpp was created, and its based on the usage of memory mapping, for dynamic exchange of information 	related to the racket and the sphere positions. Basic intelligence 	inserted, as the racket only follows sphere to establish its 	current position.
	+	Included DatosMemCompartida.h, which acts as a bridge in between Bot.cpp and Mundo.cpp.
	+	Added new funcionalities to Mundo.cpp, as both Logger.cpp & Bot.cpp requires it to work.
	+	Added extra funcionality: game ends after one of the players reach 3 points.
	+	Necessary changes implemented in other files, such as 	CMakeLists.txt, to enable multiple executions of the three main 	elements of the project: ./logger, ./tenis, ./bot (in that order).
	+	New changes after updated, merged to master branch.


*	V2.X-- 22/10/2020.
	+	Changes in Esfera.cpp, Raqueta.cpp, Esfera.h & Raqueta.h commited.
	+	Sphere & rackets movement added.
	+	Included extra feature: sphere's radius changes during gameplay.
	+	Readme file created. It contains playing instructions.
	+	Updated Changelog.


*	V1.X-- 16/10/2020.
	+	Changelog file created.
	+	First usage of main git commands (clone, push, add, rm...)


#####	Updated Changelog.

